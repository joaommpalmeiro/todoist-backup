# todoist-backup

A script to export completed items (tasks) from [Todoist](https://todoist.com/).

- [Python + HTTPX version](python-httpx)
- [Python + Requests version](python-requests)

## References

- https://developer.todoist.com/sync/v9/#get-all-completed-items
- https://developer.todoist.com/sync/v9/#authorization
- https://github.com/Doist/todoist-api-python
- https://todoist.com/pt-BR/help/articles/view-completed-tasks
