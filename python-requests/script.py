import json
import os

import requests
from funcy import project

COMPLETED_URL = "https://api.todoist.com/sync/v9/completed/get_all"
TASK_URL = "https://api.todoist.com/sync/v9/items/get"
YEAR = 2023

# https://docs.python.org/3.7/library/typing.html#typing.Dict
# https://docs.python.org/3.7/library/typing.html#typing.Mapping
# https://github.com/python/typing/issues/182
# https://github.com/kevinheavey/jsonalias
def print_json(data, sort_keys=False, indent=4):
    print(json.dumps(data, sort_keys=sort_keys, indent=indent, ensure_ascii=False))


if __name__ == "__main__":
    # start_time = time.time()
    headers = {"Authorization": f"Bearer {os.environ['API_TOKEN']}"}

    # https://developer.todoist.com/sync/v9/#get-all-completed-items
    payload = {
        "project_id": os.environ["PROJECT_ID"],
        "limit": 200,
        "since": f"{YEAR}-1-1T00:00:00",
        "until": f"{YEAR}-12-31T23:59:59",
        "annotate_notes": False,
    }
    # https://requests.readthedocs.io/en/latest/user/quickstart/
    # Alternative: https://www.python-httpx.org/quickstart/ + https://www.python-httpx.org/async/
    r = requests.get(COMPLETED_URL, headers=headers, params=payload)

    tasks = r.json()["items"]
    # print_json(tasks)

    tasks_to_save = []
    for task in tasks:
        payload = {"item_id": task["task_id"], "all_data": False}
        r = requests.get(TASK_URL, headers=headers, params=payload)

        task_data = r.json()["item"]
        # print_json(task_data)

        tasks_to_save.append(
            project(task_data, ["added_at", "completed_at", "content", "id", "labels"])
        )

        print(f"{task['task_id']} ✔")

    with open(f"data/completed_tasks_{YEAR}.json", "w") as f:
        json.dump(tasks_to_save, f, ensure_ascii=False, indent=4)
        f.write("\n")

    print("All done!")
    # print(f"{time.time() - start_time} seconds")
