# todoist-backup (Python + [Requests](https://requests.readthedocs.io/en/latest/) version)

## Development

- `pipenv install --dev` or `pipenv install` ([documentation](https://pipenv.pypa.io/en/latest/basics/#pipenv-install))
- `pipenv shell`
- `python script.py`
- `isort --profile black script.py && black script.py`

## Notes

- https://pipenv.pypa.io/en/latest/advanced/#automatic-loading-of-env
- https://pipenv.pypa.io/en/latest/changelog/#pipenv-2022-10-9-2022-10-09 (`python_version` + `python_full_version`)
- https://pipenv.pypa.io/en/latest/basics/#specifying-versions-of-python
- https://stackoverflow.com/questions/3420122/filter-dict-to-contain-only-certain-keys + https://github.com/Suor/funcy
- `pipenv install requests funcy` + `pipenv install --dev black isort`
- `exit` + `pipenv --rm`
- `pipenv install --python 3.7` or `pipenv --python 3.7` (it does not create the lock file)
- `code . &`
