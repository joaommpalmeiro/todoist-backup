# todoist-backup (Python + [HTTPX](https://www.python-httpx.org/) version)

## Development

- `pipenv install --dev` or `pipenv install` ([documentation](https://pipenv.pypa.io/en/latest/basics/#pipenv-install))
- `pipenv shell`
- `python script.py`
- `isort --profile black script.py && black script.py`

## Notes

- `pipenv install httpx funcy` + `pipenv install --dev black isort`
