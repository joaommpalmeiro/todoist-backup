import asyncio
import json
import os

import httpx
from funcy import project

BASE_URL = "https://api.todoist.com/sync/v9"
COMPLETED_ROUTE = "completed/get_all"
ITEM_ROUTE = "items/get"
YEAR = 2023
HEADERS = {"Authorization": f"Bearer {os.environ['API_TOKEN']}"}


async def get_item(client, params):
    response = await client.get(ITEM_ROUTE, params=params)
    item_data = response.json()["item"]

    print(f"{params['item_id']} ✔")

    return project(item_data, ["added_at", "completed_at", "content", "id", "labels"])


async def main():
    # https://www.python-httpx.org/api/#asyncclient
    # https://www.python-httpx.org/quickstart/
    async with httpx.AsyncClient(base_url=BASE_URL, headers=HEADERS) as client:
        params = {
            "project_id": os.environ["PROJECT_ID"],
            "limit": 200,
            "since": f"{YEAR}-1-1T00:00:00",
            "until": f"{YEAR}-12-31T23:59:59",
            "annotate_notes": False,
        }
        response = await client.get(COMPLETED_ROUTE, params=params)

        items = response.json()["items"]

        # https://www.twilio.com/blog/asynchronous-http-requests-in-python-with-httpx-and-asyncio
        # https://stackoverflow.com/a/73302844
        # https://docs.python.org/3.7/library/asyncio-task.html#asyncio.gather
        # https://docs.python.org/3.7/library/asyncio-task.html#awaitables
        # https://stackoverflow.com/questions/47493369/differences-between-futures-in-python3-and-promises-in-es6
        # https://docs.python.org/3.7/library/asyncio-future.html#asyncio.ensure_future
        # asyncio: Tasks; Todoist: Items (a.k.a. tasks)
        tasks = [
            asyncio.ensure_future(
                get_item(client, {"item_id": item["task_id"], "all_data": False})
            )
            for item in items
        ]

        items_to_save = await asyncio.gather(*tasks)

    with open(f"data/completed_tasks_{YEAR}.json", "w") as f:
        json.dump(items_to_save, f, ensure_ascii=False, indent=4)
        f.write("\n")

    print("All done!")


if __name__ == "__main__":
    # start_time = time.time()
    asyncio.run(main())
    # print(f"{time.time() - start_time} seconds")
